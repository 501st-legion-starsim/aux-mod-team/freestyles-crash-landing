[
    QGVARMAIN(damageTreshold),
    "SLIDER",
    ["Hull damage cap in %", "Hull damage aircraft can receive (in %)"],
    ["Freestyles Crash Landing", "Damage Thresholds"],
    [0, 99, 99, 0],
    1
] call CBA_fnc_addSetting;

[
    QGVARMAIN(stateThreshold),
    "SLIDER",
    ["Excess damage cap", "Excess damage aircraft can withstand"],
    ["Freestyles Crash Landing", "Damage Thresholds"],
    [1, 100, 10, 0],
    1
] call CBA_fnc_addSetting;

[
    QGVARMAIN(ignoreNonPlayerVehicles),
    "CHECKBOX",
    ["Ignore vehicles with no players in them", "If enabled, excludes any AI-only vehicles from the script."],
    ["Freestyles Crash Landing", "Ignore NPC vehicles"],
    [true],
    1
] call CBA_fnc_addSetting;

[
    QGVARMAIN(ejectionSystem),
    "CHECKBOX",
    ["Enable ejection of passengers", "If enabled passengers of the aircraft can get eject out of the aircraft when impacting the ground"],
    ["Freestyles Crash Landing", "Impact Ejection"],
    [true],
    1
] call CBA_fnc_addSetting;

[
    QGVARMAIN(ejectionProp),
    "SLIDER",
    ["Propability of ejection in %", "The propability with which a passanger is ejected when impact if force full enough (in %)"],
    ["Freestyles Crash Landing", "Impact Ejection"],
    [0, 100, 33, 0],
    1
] call CBA_fnc_addSetting;

[
    QGVARMAIN(gForceThreshold),
    "SLIDER",
    ["G-Force Thresholds", "Minimal strenght of G-Forces required to eject passangers"],
    ["Freestyles Crash Landing", "Impact Ejection"],
    [0, 100, 5, 0],
    1
] call CBA_fnc_addSetting;

[
    QGVARMAIN(debug),
    "CHECKBOX",
    ["Enable debug output", "Enables certain debug outputs of the scripts, only for development purposes."],
    ["Freestyles Crash Landing", "Development Settings"],
    [false],
    1
] call CBA_fnc_addSetting;
