#include "..\script_component.hpp"

/*
 * Author: Freestyle_Build
 * Handle the crashes of air vehicles using the handle damage eventhandler, returns new damage depending on state of the vehicle.
 *
 * Arguments:
 * 0: Aircraft <OBJECT>
 * 1: Hit Selection <STRING>
 * 2: Damage <SCALAR>
 *
 * Return Value:
 * 0: Damage <SCALAR>
 *
 * Example:
 * _aircraft addEventHandler ["HandleDamage", {call FUNC(handleDamage)}];
 *
 */

params ["_aircraft", "_selection", "_damage"];

private _stateThreshold = GVARMAIN(stateThreshold);
private _damageThreshold = GVARMAIN(damageTreshold) / 100;
private _state = _aircraft getVariable [QGVARMAIN(state), 0];
private _ignoreNonPlayerVehicles = GVARMAIN(ignoreNonPlayerVehicles);
private _playerInVehicle = true;

if (_ignoreNonPlayerVehicles) then
{
	_playerInVehicle = {
		if (_x in allPlayers) exitWith { true }; 
		false;
	} forEach (crew _aircraft);
};

if (_playerInVehicle) then
{
	//this is the check for going into the "window"
	if ((_stateThreshold > _state) and (alive _aircraft)) then 
	{
		if (_selection == "") then 
		{
			//this is the check to see if we're still in the survivable crashes "window"
			if (_damage > _damageThreshold) then 
			{
				private _newState = _state + _damage - (_aircraft getHit "");
				[_aircraft, QGVARMAIN(state), _newState] call CBA_fnc_setVarNet;

				if (!(_aircraft getVariable [QGVARMAIN(effects), false])) then 
				{

					[_aircraft] remoteExecCall [QFUNC(effects), 0, true];
					[_aircraft, QGVARMAIN(effects), true] call CBA_fnc_setVarNet;

					[_aircraft] remoteExec [QFUNC(resetDamage), 2];
					
					if(GVARMAIN(ejectionSystem)) then 
					{
						[_aircraft] remoteExec [QFUNC(impactEjection), 2];
					};
				};
				_aircraft setFuel 0;
				//diag_log "501FSCL: Result Set 1";
				//diag_log format ["501FSCL: State: %1, NewState: %2 Effect: %3, Damage: %4, _damageThreshhold: %5", _state, _aircraft getVariable [QGVARMAIN(state), 0], (_aircraft getVariable [QGVARMAIN(effects), false]), _damage, _damageThreshold];
				_damageThreshold;
			} 
			else 
			{
				//diag_log "501FSCL: Result Set 2";
				//diag_log format ["501FSCL: State: %1, NewState: %2 Effect: %3, Damage: %4, _damageThreshhold: %5", _state, _aircraft getVariable [QGVARMAIN(state), 0], (_aircraft getVariable [QGVARMAIN(effects), false]), _damage, _damageThreshold];
				_damage;
			};
		} 
		else 
		{
			//diag_log "501FSCL: Result Set 3";
			//diag_log format ["501FSCL: State: %1, NewState: %2 Effect: %3, Damage: %4, _damageThreshhold: %5", _state, _aircraft getVariable [QGVARMAIN(state), 0], (_aircraft getVariable [QGVARMAIN(effects), false]), _damage, _damageThreshold];
			_damage;
		};
	}
	else 
	{
		//diag_log "501FSCL: Result Set 4";
		//diag_log format ["501FSCL: State: %1, NewState: %2 Effect: %3, Damage: %4, _damageThreshhold: %5", _state, _aircraft getVariable [QGVARMAIN(state), 0], (_aircraft getVariable [QGVARMAIN(effects), false]), _damage, _damageThreshold];
		_damage;
	};
}
else
{
	//diag_log "501FSCL: Result Set 5";
	//diag_log format ["501FSCL: State: %1, NewState: %2 Effect: %3, Damage: %4, _damageThreshhold: %5", _state, _aircraft getVariable [QGVARMAIN(state), 0], (_aircraft getVariable [QGVARMAIN(effects), false]), _damage, _damageThreshold];
	_damage;
};


